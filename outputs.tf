output "cluster_members" {
  value       = aws_rds_cluster.postgresql.cluster_members
  description = "Cluster Members"
}

output "cluster_endpoint" {
  value       = aws_rds_cluster.postgresql.endpoint
  description = "Cluster Endpoint"
}