# Set variable values declared in variables.tf
aws_profile = "default"
aws_region  = "us-west-2"
db_port     = 5432
cidr_block  = "0.0.0.0/0"
cluster_id  = "aurora-cluster-demo"
eng_mode    = "serverless"
eng_type    = "aurora-postgresql"
db_name     = "jangomart"
db_user     = "dadmin"
db_pass     = "NOT_the_admin1239$!"