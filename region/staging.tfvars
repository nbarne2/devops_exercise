# Set variable values declared in variables.tf
aws_profile         = "default"
aws_region          = "us-east-2"
product_code_tag    = "STG2318"
inventory_code_tag  = "Noah_Staging_S3"
s3_bucket           = "tf-bucket4587"
instance_type       = "t2.micro"
ami_flavor          = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
environment         = "staging"