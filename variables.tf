# Terraform variables
variable "aws_profile" {
  type        = string
  description = "Name of the AWS profile to which deployment is done"
  default     = "default"
}
variable "aws_region" {
  type        = string
  description = "Code of the region to which deployment is done, eg: us-west-2"
  default     = "us-west-2"
}
variable "db_port" {
  type        = number
  description = "DB port the cluster allows connections to"
  default     = 5432
}
variable "cidr_block" {
  type        = string
  description = "VPC block"
  default     = "0.0.0.0/0"
}
variable "cluster_id" {
  type        = string
  description = "Cluster identifier"
  default     = "aurora-cluster-demo"
}
variable "eng_mode" {
  type        = string
  description = "Type of RDS cluster"
  default     = "serverless"
}
variable "eng_type" {
  type        = string
  description = "Specific type of SQL for RDS"
  default     = "aurora-postgresql"
}
variable "db_name" {
  type        = string
  description = "DB name"
  default     = "jangomart"
}
variable "db_user" {
  type        = string
  description = "DB username"
  default     = "dadmin"
}
variable "db_pass" {
  type        = string
  description = "DB password"
  default     = "NOT_the_admin1239$!"
}