# DevOps Interview Exercise

This project uses Terraform to create the following resources in AWS:  
- A serverless PostgreSQL RDS cluster
 
## Local prerequisites
Install AWS CLI locally and run 
```bash
$ aws configure
```
This will prompt you to answer some questions.

Terraform will need to authenticate to AWS, so an IAM user will need to be created for Terraform to run as.
Terraform will use the AWS environment variables stored at `~/.aws/credentials`

Link to [AWS Configuration and credential file settings](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)

##  Terraform Switches

The `-auto-approve` switch assumes yes and never asks "Do you want to perform these actions?"

Install Terraform v0.13.2 and run these:  
```bash
$ terraform fmt
$ terraform init
$ terraform plan -var-file=region/prod.tfvars
$ terraform apply -var-file=region/prod.tfvars -auto-approve
```
`Warning` - Be sure to verify that upgrading to Terraform v0.13.2 will not break any older scripts

Login to the console and go to RDS, and you can check your newly created RDS cluster.
 
Alternatively, you can check the CLI and ensure the RDS cluster configuration is accurate.
 
## Environment destruction
 
Run this when you are ready to tear everything down.
```bash
$ terraform destroy -var-file=region/prod.tfvars -auto-approve
```
