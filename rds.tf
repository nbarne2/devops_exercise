resource "aws_rds_cluster" "postgresql" {
  cluster_identifier      = var.cluster_id
  engine_mode             = var.eng_mode
  engine                  = var.eng_type
  availability_zones      = ["us-west-2a", "us-west-2b", "us-west-2c"]
  database_name           = var.db_name
  master_username         = var.db_user
  master_password         = var.db_pass
  backup_retention_period = 5
  preferred_backup_window = "07:00-09:00"
  skip_final_snapshot     = true
  #  final_snapshot_identifier = "foo"

  scaling_configuration {
    auto_pause               = true
    max_capacity             = 4
    min_capacity             = 2
    seconds_until_auto_pause = 300
    timeout_action           = "ForceApplyCapacityChange"
  }

  vpc_security_group_ids = [aws_security_group.ingress_allow_db.id]

}
