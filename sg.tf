# Declare default VPC
resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

# Create security group for VPC to open inbound DB traffic
resource "aws_security_group" "ingress_allow_db" {
  name   = "ingress_allow_db"
  vpc_id = aws_default_vpc.default.id

  ingress {
    from_port   = var.db_port
    to_port     = var.db_port
    protocol    = "tcp"
    cidr_blocks = [var.cidr_block]
  }

  # Removes the default rule
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.cidr_block]
  }

}